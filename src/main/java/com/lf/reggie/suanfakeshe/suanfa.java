//package com.lf.reggie.suanfakeshe;
//
//import java.util.Random;
//import java.util.Scanner;
//
///**
// * @Author 林峰
// * @Date
// * @Version 1.0
// *
// */
//public class suanfa {
//    public static void main(String[] args) {
//        Scanner in = new Scanner(System.in);
//        System.out.print("输入点的数量:");
//        int n = in.nextInt();
//
//        Random r = new Random();
//        PointX[] X = new PointX[n];
//        for (int i = 0; i < n; i++) {
//            X[i] = new PointX();
//            X[i].ID = i;
//            X[i].x = r.nextFloat() ;
//            X[i].y = r.nextFloat() ;
//        }
//
//        PointX a = new PointX(), b = new PointX();
//        float d = Float.MAX_VALUE;
//        closestPair(X, n, a, b, d);
//
//
//    }
//
//    // PointX类表示按x坐标排序的点
//    static class PointX implements Comparable<PointX> {
//        int ID;     // 点编号
//        float x, y; // 坐标
//
//        public int compareTo(PointX p) {
//            return Float.compare(x, p.x);
//        }
//    }
//
//    // PointY类表示按y坐标排序的点
//    static class PointY implements Comparable<PointY> {
//        int p;     //同一点在数组x中的坐标
//        float x, y;
//
//        public int compareTo(PointY p) {
//            return Float.compare(y, p.y);
//        }
//    }
//
//    // 计算两点之间的欧式距离
//    static float dis(PointX u, PointX v)  {
//        float dx = u.x - v.x;
//        float dy = u.y - v.y;
//        return (float) Math.sqrt(dx * dx + dy * dy);
//    }
//
//    // 最接近点对搜索
//    static boolean closestPair(PointX[] X, int n, PointX a, PointX b, float d) {
//        if (n < 2) return false;   //至少两个点
//
//        PointX[] tmpX = new PointX[n];
//        mergeSort(X, tmpX, 0, n - 1);
//
//        PointY[] Y = new PointY[n];
//        for (int i = 0; i < n; i++) {
//            Y[i] = new PointY();
//            Y[i].p = i;
//            Y[i].x = X[i].x;
//            Y[i].y = X[i].y;
//        }
//        PointY[] tmpY = new PointY[n];
//        mergeSort(Y, tmpY, 0, n - 1);
//
//        closest(X, Y, tmpY, 0, n - 1, a, b, d);
//
//        return true;
//    }
//
//    // 搜索最接近点对
//    static void closest(PointX[] X, PointY[] Y, PointY[] Z, int l, int r,
//                        PointX a, PointX b, float d) {
//        if (r - l == 1) {  //两点情况
//            a = X[l];
//            b = X[r];
//            d = dis(X[l], X[r]);
//            System.out.println("最接近点对为:(" + a.x + ", " + a.y + ") 和 ("
//                    + b.x + ", " + b.y + ")");
//            System.out.println("两点之间的距离为: " + d);
//            return;
//        }
//        else if (r - l == 2) {  //三点情况
//            float d1 = dis(X[l], X[l + 1]);
//            float d2 = dis(X[l + 1], X[r]);
//            float d3 = dis(X[l], X[r]);
//            if (d1 <= d2 && d1 <= d3) {
//                a = X[l];
//                b = X[l + 1];
//                d = d1;
//            } else if (d2 <= d3) {
//                a = X[l + 1];
//                b = X[r];
//                d = d2;
//            } else {
//                a = X[l];
//                b = X[r];
//                d = d3;
//            }
//            System.out.println("最接近点对为:(" + a.x + ", " + a.y + ") 和 ("
//                    + b.x + ", " + b.y + ")");
//            System.out.println("两点之间的距离为: " + d);
//            return;
//        }
//
//        // 多于3个点,分治
//        int m = (l + r) / 2;
//        int f = l, g = m + 1;
//
//        // 分割点集,左半边在Y[l...m]中,右半边在Y[m+1...r]中
//        for (int i = l; i <= r; i++) {
//            if (Y[i].p <= m)
//                Z[f++] = Y[i];
//            else
//                Z[g++] = Y[i];
//        }
//
//        PointX al = new PointX(), bl = new PointX();
//        float dl = d;
//        closest(X, Z, Y, l, m, al, bl, dl);
//
//        PointX ar = new PointX(), br = new PointX();
//        float dr = d;
//        closest(X, Z, Y, m + 1, r, ar, br, dr);
//
//        // 比较左右两部分的最短距离,更新最接近点对及其距离
//        if (dr < dl) {
//            a = ar;
//            b = br;
//            d = dr;
//        } else {
//            a = al;
//            b = bl;
//            d = dl;
//        }
//
//        // 搜索跨分界线的点,更新最接近点对及其距离
//        int k = l;
//        for (int i = l; i <= r; i++) {
//            if (Math.abs(X[m].x - Y[i].x) < d)
//                Z[k++] = Y[i];
//        }
//
//        for (int i = l; i < k; i++) {
//            for (int j = i + 1; j < k && Z[j].y - Z[i].y < d; j++) {
//                float dp = dis(X[Z[i].p], X[Z[j].p]);
//                if (dp < d) {
//                    d = dp;
//                    a = X[Z[i].p];
//                    b = X[Z[j].p];
//                }
//            }
//        }
//        System.out.println("最接近点对为:(" + a.x + ", " + a.y + ") 和 ("
//                + b.x + ", " + b.y + ")");
//        System.out.println("两点之间的距离为: " + d);
//    }
//
//    // 归并排序
//    static <T extends Comparable<T>> void mergeSort(T[] a, T[] b, int l, int r) {
//        if (l < r) {
//            int m = (l + r) / 2;
//            mergeSort(a, b, l, m);        //排序左半部分
//            mergeSort(a, b, m + 1, r);   //排序右半部分
//            merge(a, b, l, m, r);        //合并
//            System.arraycopy(b, l, a, l, r - l + 1);  //复制回原数组
//        }
//    }
//
//    // 合并两个有序数组
//    static <T extends Comparable<T>> void merge(T[] c, T[] d, int l, int m, int r){
//        int i = l, j = m + 1, k = l;
//        while (i <= m && j <= r) {
//            if (c[i].compareTo(c[j]) <= 0) {
//                d[k++] = c[i++];
//            } else {
//                d[k++] = c[j++];
//            }
//        }
//
//        if (i > m) {
//            while (j <= r) {
//                d[k++] = c[j++];
//            }
//        } else {
//            while (i <= m) {
//                d[k++] = c[i++];
//            }
//        }
//    }
//}
