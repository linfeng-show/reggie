package com.lf.reggie.suanfakeshe;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

/**
 * @Author 林峰
 * @Date
 * @Version 1.0
 */
public class Near {


    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("输入点的数量:");
        int n = in.nextInt();

        Random r = new Random();
        PointX[] X = new PointX[n];
        for (int i = 0; i < n; i++) {
            X[i] = new PointX();
            X[i].ID = i;
            X[i].x = r.nextFloat();
            X[i].y = r.nextFloat();
        }


        float[][] result = new float[n][n];
//        R数组作为结果，方便排序
        R[] out = new R[100];
        closestPair(X,n,result);

        float min = 999;
        int m1 = 0,m2 = 0;
        int k = 0;
//        从二维数组里找最小距离,并将有效数据拷贝到out中
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if(result[i][j] != 0){
                    if(result[i][j] < min){
                        min = result[i][j];
                        m1 = i;
                        m2 = j;
                    }
                    out[k++] = new R(i,j,result[i][j]);
                }
            }
        }
        System.out.println("坐标点 ("+X[m1].x + " " +X[m1].y + ") ("+X[m2].x +" " + X[m2].y+") 之间的距离最短，且为"
                +min);

        System.out.println();

        Arrays.sort(out,0,k);

        for (int i = 0; i < k;i+=2) {
                    System.out.println("坐标点 ("+X[out[i].p1].x + " " +X[out[i].p1].y + ") ("+X[out[i].p2].x +" " + X[out[i].p2].y+") 之间的距离为"
                            +out[i].d);

        }
    }
    // PointX类表示按x坐标排序的点
    static class PointX implements Comparable<PointX> {
        int ID;     // 点编号
        float x, y; // 坐标

        public int compareTo(PointX p) {
            return Float.compare(x, p.x);
        }
    }

    // PointY类表示按y坐标排序的点
    static class PointY implements Comparable<PointY> {
        int p;     //同一点在数组x中的坐标
        float x, y;

        public int compareTo(PointY p) {
            return Float.compare(y, p.y);
        }
    }

    //距离结果
    static class R implements Comparable<R>{
        int p1,p2;
        float d;

        public R(int p1, int p2, float d) {
            this.p1 = p1;
            this.p2 = p2;
            this.d = d;
        }

        @Override
        public int compareTo(R r) {
            return Float.compare(this.d,r.d);
        }

        @Override
        public String toString() {
            return "R{" +
                    "p1=" + p1 +
                    ", p2=" + p2 +
                    ", d=" + d +
                    '}';
        }
    }

    // 计算两点之间的距离
    static float dis(PointX u, PointX v)  {
        float dx = u.x - v.x;
        float dy = u.y - v.y;
        return (float) Math.sqrt(dx * dx + dy * dy);
    }

    // 最接近点对搜索
    static void closestPair(PointX[] X, int n,float[][] result) {
        if(n < 2) return;

        PointX[] tmpX = new PointX[n];
        mergeSort(X, tmpX, 0, n - 1);

        PointY[] Y = new PointY[n];
        for (int i = 0; i < n; i++) {
            Y[i] = new PointY();
            Y[i].p = i;
            Y[i].x = X[i].x;
            Y[i].y = X[i].y;
        }
        PointY[] tmpY = new PointY[n];
        mergeSort(Y, tmpY, 0, n - 1);
        closest(X, Y, tmpY, 0, n - 1, result);

    }

//    递归求解最短距离d
    static void closest(PointX[] X, PointY[] Y, PointY[] Z, int l, int r,
                        float[][] result){
        if(r - l == 1){
            //两点情况
            result[l][r] = dis(X[l],X[r]);
            result[r][l] = result[l][r];
            return;
        }
        else if(r - l == 2){
            result[l][l + 1] = dis(X[l],X[l + 1]);
            result[l + 1][r] = dis(X[l + 1],X[r]);
            result[l][r] = dis(X[l],X[r]);

            result[l + 1][l] = result[l][l + 1];
            result[r][l + 1] = result[l + 1][r] ;
            result[r][l] = result[l][r];
            return;
        }

        // 多于3个点,分治
        int m = (l + r) / 2;
        int f = l, g = m + 1;

        // 分割点集,左半边在Z[l...m]中,右半边在Z[m+1...r]中
        for (int i = l; i <= r; i++) {
            if (Y[i].p <= m)
                Z[f++] = Y[i];
            else
                Z[g++] = Y[i];
        }

        //左半部分
        closest(X, Z, Y, l, m, result);
        //右半部分
        closest(X, Z, Y, m + 1, r, result);

        float min = 99999;
        for (int i = 0; i < r + 1; i++) {
            for (int j = 0; j < r + 1; j++) {
                if(result[i][j] < min && result[i][j] != 0){
                    min = result[i][j];
                }
            }
        }

        // 搜索跨分界线的点
        int k = l;
        for (int i = l; i <= r; i++) {
            if (Math.abs(X[m].x - Y[i].x) < min)
                Z[k++] = Y[i];
        }
//        将Z[i] 和 区域R中的点求距离
        for (int i = l; i < k; i++) {
            for (int j = i + 1; j < k && Z[j].y - Z[i].y < min; j++) {
                result[Z[i].p][Z[j].p] = dis(X[Z[i].p], X[Z[j].p]);
                result[Z[j].p][Z[i].p] = result[Z[i].p][Z[j].p];
            }
        }

    }

    // 归并排序
    static <T extends Comparable<T>> void mergeSort(T[] a, T[] b, int l, int r) {
        if (l < r) {
            int m = (l + r) / 2;
            mergeSort(a, b, l, m);        //排序左半部分
            mergeSort(a, b, m + 1, r);   //排序右半部分
            merge(a, b, l, m, r);        //合并
            System.arraycopy(b, l, a, l, r - l + 1);  //复制回原数组
        }
    }

    // 合并两个有序数组
    static <T extends Comparable<T>> void merge(T[] c, T[] d, int l, int m, int r){
        int i = l, j = m + 1, k = l;
        while (i <= m && j <= r) {
            if (c[i].compareTo(c[j]) <= 0) {
                d[k++] = c[i++];
            } else {
                d[k++] = c[j++];
            }
        }

        if (i > m) {
            while (j <= r) {
                d[k++] = c[j++];
            }
        } else {
            while (i <= m) {
                d[k++] = c[i++];
            }
        }
    }
}
