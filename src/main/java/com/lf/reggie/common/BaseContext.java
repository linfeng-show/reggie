package com.lf.reggie.common;

/**
 * @Author 林峰
 * @Date
 * @Version 1.0
 * 线程内共享数据
 */
public class BaseContext {
    private static ThreadLocal<Long> threadLocal = new ThreadLocal<>();

//    设置数据
    public static  void setCurrentId(Long id){
        threadLocal.set(id);
    }

//    取数据
    public static  Long getCurrentId(){
        return threadLocal.get();
    }
}
