package com.lf.reggie.common;

import com.lf.reggie.domain.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLIntegrityConstraintViolationException;

/**
 * @Author 林峰
 * @Date
 * @Version 1.0
 * 全局异常捕获
 */
@ControllerAdvice(annotations = {RestController.class, Controller.class})
@ResponseBody
@Slf4j
public class GlobalExceptionHandler {


//捕获员工已存在异常
    @ExceptionHandler(SQLIntegrityConstraintViolationException.class)
    public R<String> exceptionHandler(SQLIntegrityConstraintViolationException ex){
        /*
        捕获异常后的异常处理方法
         */
        log.error(ex.getMessage());
        if(ex.getMessage().contains("Duplicate entry")){
            return R.error("该字段已存在，添加失败");
        }
        return R.error("未知错误");

    }
//    捕获关联删除异常
@ExceptionHandler(CustomException.class)
public R<String> exceptionHandler(CustomException ex){
        /*
        捕获异常后的异常处理方法
         */
    log.error(ex.getMessage());

    return R.error(ex.getMessage());

}

}
