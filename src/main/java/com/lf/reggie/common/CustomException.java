package com.lf.reggie.common;



/**
 * @Author 林峰
 * @Date
 * @Version 1.0
 * 自定义删除异常
 */
public class CustomException extends RuntimeException {
    public CustomException(String message){
        super(message);
    }
}
