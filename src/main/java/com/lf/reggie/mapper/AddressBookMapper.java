package com.lf.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lf.reggie.domain.AddressBook;
import org.apache.ibatis.annotations.Mapper;


/**
 * @Author 林峰
 * @Date
 * @Version 1.0
 */
@Mapper
public interface AddressBookMapper extends BaseMapper<AddressBook> {
}
