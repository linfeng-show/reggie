package com.lf.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lf.reggie.domain.OrderDetail;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author 林峰
 * @Date
 * @Version 1.0
 */
@Mapper
public interface OrderDetailMapper extends BaseMapper<OrderDetail> {
}
