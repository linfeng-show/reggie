package com.lf.reggie.config;

import com.lf.reggie.common.JacksonObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


import java.util.List;

/**
 * @Author 林峰
 * @Date
 * @Version 1.0
 */
@Slf4j
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    @Override
    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        /**
        * @Description:
        * @Params: 扩展消息转换器
        * @Return void
        */

//      创建消息转换器对象
        MappingJackson2HttpMessageConverter mjhmc = new MappingJackson2HttpMessageConverter();

//      设置对象转换器，把我们自定义好的转换器传进去
        mjhmc.setObjectMapper(new JacksonObjectMapper());
//       将设置好的对象转换器加入到我们mvc框架的转换器集合中
        converters.add(0,mjhmc);

    }

}


//    /**
//     * 功能描述:
//     * 配置静态资源,避免静态资源请求被拦截
//     *
//     * @auther:
//     * @date:
//     * @param:
//     * @return:
//     */
//    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//        registry.addResourceHandler("/static/**")
//                .addResourceLocations("classpath:/static/");
//        registry.addResourceHandler("/templates/**")
//                .addResourceLocations("classpath:/templates/");
//        super.addResourceHandlers(registry);
//    }
