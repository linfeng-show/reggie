package com.lf.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lf.reggie.domain.Employee;
import com.lf.reggie.domain.R;
import com.lf.reggie.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;

/**
 * @Author 林峰
 * @Date
 * @Version 1.0
 *
 */
@Slf4j
@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;


//登录controller
    @PostMapping("/login")
    //因为前端发过来是Json数据，所以需要加 @RequestBody接收数据
    public R<Employee> login(HttpServletRequest request, @RequestBody Employee employee) {
        /**
         * @Description:
         * @Params: [request, employee]
         * request 之后用来获取session
         * employee 前端传入的账号密码
         * @Return com.lf.reggie.domain.R<com.lf.reggie.domain.Employee>
         */

//        1.前端提交的passowrd进行加密处理
        String password = employee.getPassword();
        password = DigestUtils.md5DigestAsHex(password.getBytes());
//        2.根据用户名查询数据库
//        首先需要创建条件构造器，详情见SpringBoot学习笔记（一），条件查询
        LambdaQueryWrapper<Employee> queryWrapper = new LambdaQueryWrapper<>();
//        构造条件,列名 + 值
        queryWrapper.eq(Employee::getUsername, employee.getUsername());

        Employee emp = employeeService.getOne(queryWrapper);

//        3.如果值为空，说明用户名不存在
        if (emp == null) {
            return R.error("用户名不存在");
        }
//        4.值不为空，则比对密码
        if (!emp.getPassword().equals(password)) {
            return R.error("密码错误");
        }
//        5.判断用户是否被禁用
        if (emp.getStatus() == 0) {
            return R.error("该用户已被禁用");
        }
//        6.上面都通过了，说明可以登录,把emp对象放入session，方便其他调用
        HttpSession session = request.getSession();
        session.setAttribute("employee", emp.getId());

        return R.success(emp);
    }

    @PostMapping("/logout")
    //员工退出
    public R<String> logout(HttpServletRequest request){
        /**
        * @Description:
        * @Params: [request]
         * request 用于获取sesssion
        * @Return com.lf.reggie.domain.R<java.lang.String>
        */
//        从session里面移除之前存的登录信息
        request.removeAttribute("employee");

        return R.success("退出成功");
    }

@PostMapping

//    新增员工代码
    public R<String> save(HttpServletRequest request,@RequestBody Employee employee){
        /**
        * @Description:
        * @Params: [request, employee]
         * request 获取session得到操作人信息，
         * employee 前端传的员工信息
        * @Return com.lf.reggie.domain.R<java.lang.String>
        */
        log.info("新增员工信息{}",employee);
//        md5加密密码，初始密码为123456
        employee.setPassword(DigestUtils.md5DigestAsHex("123456".getBytes()));

//       给我们新增的信息加点料： 创建时间，更新时间，创建人，更新人
//        employee.setCreateTime(LocalDateTime.now());
//        employee.setUpdateTime(LocalDateTime.now());

//        HttpSession session = request.getSession();
//        Long id = (Long) session.getAttribute("employee");

//        employee.setCreateUser(id);
//        employee.setUpdateUser(id);

//        调用方法保存数据库
        employeeService.save(employee);
//        System.out.println(employee);
        return R.success("新增员工成功");
    }

    //    分页查询
    @GetMapping("/page")
    public R<Page> page(int page, int pageSize, String name) {

//        分页构造器：
        Page pageInfo = new Page(page, pageSize);
//        条件构造器：
        LambdaQueryWrapper<Employee> queryWrapper = new LambdaQueryWrapper<>();
//        加条件：
        queryWrapper.like(StringUtils.isNotEmpty(name), Employee::getName, name);
//        排序条件：
        queryWrapper.orderByDesc(Employee::getUpdateTime);

//        调用service执行查询,自动封装到pageInfo
        employeeService.page(pageInfo, queryWrapper);

        return R.success(pageInfo);
    }

//    修改员工信息
    @PutMapping
    public R<String> updata(HttpServletRequest request,@RequestBody Employee employee){
        HttpSession session = request.getSession();
        Long empID = (Long)session.getAttribute("employee");
//        更新时间，和操作人
//        employee.setUpdateTime(LocalDateTime.now());
//        employee.setUpdateUser(empID);
//        调用service
        employeeService.updateById(employee);

        return R.success("成功");
    }

    @GetMapping("/{id}")
    public R<Employee> getById(@PathVariable Long id){
        /**
        * @Description:
        * @Params: [id]
        * @Return com.lf.reggie.domain.R<com.lf.reggie.domain.Employee>
         *     根据前端传给的id查询用户
        */
        log.info("根据id查询用户");
        Employee employee = employeeService.getById(id);
        if(employee != null){
            return R.success(employee);
        }else
            return R.error("没有找到数据");

    }

}
