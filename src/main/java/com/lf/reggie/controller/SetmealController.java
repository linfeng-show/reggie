package com.lf.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lf.reggie.domain.R;
import com.lf.reggie.domain.Setmeal;
import com.lf.reggie.dto.SetmealDto;
import com.lf.reggie.service.CategoryService;
import com.lf.reggie.service.SetMealDishService;
import com.lf.reggie.service.SetMealService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author 林峰
 * @Date
 * @Version 1.0
 */
//套餐管理
@RestController
@RequestMapping("/setmeal")
@Slf4j

public class SetmealController {
    @Autowired
    private SetMealDishService setMealDishService;

    @Autowired
    private SetMealService setMealService;

    @Autowired
    private CategoryService categoryService;

    @PostMapping
    public R<String> save(@RequestBody SetmealDto setmealDto){
        /**
        * @Description:
        * @Params: [setmealDto] 新增套餐信息
        * @Return com.lf.reggie.domain.R<java.lang.String>
         *     保存套餐信息
        */
        setMealService.saveWithMeal(setmealDto);
//        System.out.println(setmealDto.toString());
        return R.success("新增套餐成功");
    }

    @GetMapping("/page")
    public R<Page> page(int page,int pageSize,String name){
        /**
        * @Description:
        * @Params: [page, pageSize] 页码和页大小
        * @Return com.lf.reggie.domain.R<com.baomidou.mybatisplus.extension.plugins.pagination.Page>
        */

        Page<Setmeal> pageInfo = new Page<>(page,pageSize);
        Page<SetmealDto> dtopage = new Page<>();
        LambdaQueryWrapper<Setmeal> lambdaQueryWrapper = new LambdaQueryWrapper<>();

        lambdaQueryWrapper.like(name != null,Setmeal::getName,name);
        lambdaQueryWrapper.orderByDesc(Setmeal::getUpdateTime);

        setMealService.page(pageInfo,lambdaQueryWrapper);
        BeanUtils.copyProperties(pageInfo,dtopage,"records");

        List<Setmeal> pageInfoRecords = pageInfo.getRecords();

        ArrayList<SetmealDto> records = new ArrayList<>();

        for (Setmeal item :
                pageInfoRecords) {
            SetmealDto sd = new SetmealDto();
            BeanUtils.copyProperties(item,sd);

            sd.setCategoryName( categoryService.getById(item.getCategoryId()).getName());
            records.add(sd);
        }
        dtopage.setRecords(records);
        return R.success(dtopage);
    }
//  删除套餐逻辑
    @DeleteMapping
    public R<String> delete(@RequestParam List<Long> ids){

        setMealService.deleteWithMeal(ids);

        return R.success("套餐删除成功！");

    }
// 套餐展示
    @GetMapping("/list")
    public R<List<Setmeal>> list(Setmeal setmeal){
//        构造查询条件
        LambdaQueryWrapper<Setmeal> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(Setmeal::getCategoryId,setmeal.getCategoryId());
        lambdaQueryWrapper.eq(Setmeal::getStatus,setmeal.getStatus());

        List<Setmeal> list = setMealService.list(lambdaQueryWrapper);
        return R.success(list);
    }

}
