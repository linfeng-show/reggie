package com.lf.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lf.reggie.domain.Category;
import com.lf.reggie.domain.Dish;
import com.lf.reggie.domain.DishFlavor;
import com.lf.reggie.domain.R;
import com.lf.reggie.dto.DishDto;
import com.lf.reggie.service.CategoryService;
import com.lf.reggie.service.DishFlavorService;
import com.lf.reggie.service.DishService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author 林峰
 * @Date
 * @Version 1.0
 */
@RestController
@Slf4j
@RequestMapping("/dish")

public class DishController {

    @Autowired
    private DishService dishService;

    @Autowired
    private DishFlavorService dishFlavorService;

    @Autowired
    private CategoryService categoryService;

    @PostMapping
    public R<String> save(@RequestBody DishDto dishDto){
         /**
         * @Description:
         * @Params: [dishDto]
          * 新的dto，可以封装菜品信息
         * @Return com.lf.reggie.domain.R<java.lang.String>
         */
         log.info(dishDto.toString());
         dishService.saveWithFlavor(dishDto);

        return R.success("新增菜品成功");
    }

    @GetMapping("/page")
    public R<Page> page(int page,int pageSize,String name){
        Page<Dish> p = new Page<>(page,pageSize);
        Page<DishDto> pagedto = new Page<>();

//        条件构造器
        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper<>();
//        过滤条件，此处是模糊匹配查询name
        queryWrapper.like(name != null,Dish::getName,name);
//        排序条件
        queryWrapper.orderByDesc(Dish::getUpdateTime);
//        调用page方法会把页面自动封装到p里
        dishService.page(p,queryWrapper);
//        将取出的page中的除records之外的数据拷贝到pagedto，因为records要另外做修改
        BeanUtils.copyProperties(p,pagedto,"records");
//        BeanUtils.copyProperties(p,pagedto);
//        取出原始数据
        List<Dish> records = p.getRecords();
//        List<DishDto> records = pagedto.getRecords();

        List<DishDto> list = new ArrayList<>();

        for (Dish item :
                records) {

            DishDto dishDto = new DishDto();
//            把原先的数据拷贝过来，然后下面处理分类名
            BeanUtils.copyProperties(item,dishDto);
            Category byId = categoryService.getById(item.getCategoryId());

            dishDto.setCategoryName(byId.getName());
            list.add(dishDto);
        }

        pagedto.setRecords(list);

        return R.success(pagedto);
    }
//  修改页面数据回显
    @GetMapping("/{id}")
    public R<DishDto> getDish(@PathVariable Long id){
        /**
        * @Description:
        * @Params: [id] 前端传入菜品id
        * @Return com.lf.reggie.domain.R<com.lf.reggie.dto.DishDto>
         *     因为需要口味，所以返回值应该是一个dto
        */

        DishDto byIdWithFlavor = dishService.getByIdWithFlavor(id);


        return R.success(byIdWithFlavor);
    }

//    修改菜品
    @PutMapping
    public R<String> update(@RequestBody DishDto dishDto){
        /**
         * @Description:
         * @Params: [dishDto]
         * 新的dto，可以封装菜品信息
         * @Return com.lf.reggie.domain.R<java.lang.String>
         */
        log.info(dishDto.toString());
        dishService.updateWithFlavor(dishDto);

        return R.success("修改菜品成功");
    }

    @GetMapping("/list")
    public R<List<DishDto>> list(Dish dish){

//        构造查询条件
        LambdaQueryWrapper<Dish> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(dish.getCategoryId() != null,Dish::getCategoryId,dish.getCategoryId());
        lambdaQueryWrapper.orderByAsc(Dish::getSort).orderByAsc(Dish::getUpdateTime);
        lambdaQueryWrapper.eq(Dish::getStatus,1);

        List<Dish> list = dishService.list(lambdaQueryWrapper);

//        由于返回的list不含口味信息，所以继续封装成dishdto
        List<DishDto> dishDtos = new ArrayList<>();

//        BeanUtils.copyProperties(list,dishDtos);
//        log.info(dishDtos.toString());

//        for(DishDto item : dishDtos){
//            DishDto byIdWithFlavor = dishService.getByIdWithFlavor(item.getId());
//            item.setFlavors(byIdWithFlavor.getFlavors());
//        }
        for(Dish item:list){
            DishDto dto = new DishDto();
            BeanUtils.copyProperties(item,dto);
            Category category = categoryService.getById(dish.getCategoryId());
            dto.setCategoryName(category.getName());
            dto.setFlavors(dishService.getByIdWithFlavor(item.getId()).getFlavors());
            dishDtos.add(dto);
        }
        log.info(dishDtos.toString());

        return R.success(dishDtos);
    }

}
