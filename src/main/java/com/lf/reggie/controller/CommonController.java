package com.lf.reggie.controller;

import com.lf.reggie.domain.R;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.UUID;

/**
 * @Author 林峰
 * @Date
 * @Version 1.0
 */
@RestController
@RequestMapping("/common")
@Slf4j
public class CommonController {

//    将图片路径加入配置文件
    @Value("${reggie.path}")
    private String basepath;

    @PostMapping("/upload")
    public R<String> upload(MultipartFile file){
//       此处接收到的file是临时文件，需要将其转存
        log.info(file.toString());

//        利用UUID重新生成文件名,防止文件名重复
        String filename = UUID.randomUUID().toString();

        try {
//            上传的文件转存
            file.transferTo(new File(basepath + filename));
        } catch (IOException e) {
            R.error("上传失败");
        }
        return R.success(filename);
    }

    @GetMapping("/download")
    public void download(String name, HttpServletResponse response){
        /**
        * @Description:
        * @Params: [name, response]
        * @Return void
         * 文件下载
        */

        try {
//            输入流，获取图片
            FileInputStream fileInputStream = new FileInputStream(new File(basepath + name));
//            输出流，回显图片
            ServletOutputStream outputStream = response.getOutputStream();

            response.setContentType("image/jpeg");
            byte[] bytes = new byte[1024];
            int len = 0;
            while((len = fileInputStream.read(bytes)) != -1){
//                回显
                outputStream.write(bytes,0,len);
                outputStream.flush();
            }
            outputStream.close();
            fileInputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
