package com.lf.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.lf.reggie.domain.R;
import com.lf.reggie.domain.User;
import com.lf.reggie.service.UserService;
import com.lf.reggie.utils.ValidateCodeUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * @Author 林峰
 * @Date
 * @Version 1.0
 */
@RestController
@RequestMapping("/user")
@Slf4j
// 用户操作
public class UserController {

    @Autowired
    UserService userService;




    @PostMapping("/sendMsg")
    public R<String> sendMsg(@RequestBody User user, HttpSession session){
//      获取手机号
        String phone = user.getPhone();

//       生成随机验证码
        String code = ValidateCodeUtils.generateValidateCode(4).toString();
//        发送短信(由于发送短信需要额外申请互联网服务，需要花钱，此处仅作演示)
        log.info("验证码{}",code);
//       session 保存验证码
        session.setAttribute(phone,code);
        return R.success("短信发送成功！");
    }

    @PostMapping("/login")
    public R<User> login(@RequestBody Map map,HttpSession session){

        log.info(map.toString());

//        获取手机号
        String phone = map.get("phone").toString();
//        获取验证码
        String code = map.get("code").toString();
//        取session验证码
        Object session_code = session.getAttribute(phone);
        if(session_code != null && session_code.equals(code)){
            LambdaQueryWrapper<User> lambdaQueryWrapper = new LambdaQueryWrapper<>();

            lambdaQueryWrapper.eq(User::getPhone,phone);
            User user = userService.getOne(lambdaQueryWrapper);
            if(user != null){
//                虚晃
            }else{
                user = new User();
                user.setPhone(phone);
                user.setStatus(1);
                userService.save(user);
            }
            session.setAttribute("user",user.getId());
            return R.success(user);
        }
//        比对
//        另外判断是否新用户（自动注册）
        return R.error("错误！");
    }
}
