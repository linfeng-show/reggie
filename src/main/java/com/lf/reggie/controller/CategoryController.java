package com.lf.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lf.reggie.domain.Category;
import com.lf.reggie.domain.Dish;
import com.lf.reggie.domain.R;
import com.lf.reggie.service.CategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author 林峰
 * @Date
 * @Version 1.0
 * 分类管理
 */
@RestController
@RequestMapping("category")
@Slf4j
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

//    新增分类
    @PostMapping
    public R<String> save(@RequestBody Category category){
        log.info("category:{}",category);

        categoryService.save(category);
        return R.success("新增成功");
    }

    //    分页查询
    @GetMapping("/page")
    public R<Page> page(int page,int pageSize){

        Page<Category> p = new Page<>(page, pageSize);
//        条件构造器
        LambdaQueryWrapper<Category> lambdaQueryWrapper = new LambdaQueryWrapper<>();

        lambdaQueryWrapper.orderByDesc(Category::getSort);
        categoryService.page(p,lambdaQueryWrapper);

        return R.success(p);
    }

//    删除
    @DeleteMapping
    public R<String>  delete(Long ids){
        log.info("删除分类，id为：{}",ids);

        categoryService.remove(ids);
        return R.success("删除成功！");
    }

//    修改分类信息
    @PutMapping
    public R<String> update(@RequestBody Category category){
        /**
        * @Description:
        * @Params: [category]
         * 前端传入的分类信息，包括id
        * @Return com.lf.reggie.domain.R<java.lang.String>
        */
        log.info("修改分类信息：{}",category);

        categoryService.updateById(category);
        return R.success("修改分类成功");
    }

    @GetMapping("/list")
    public R<List<Category>> list(Category category){
//        条件构造器
        LambdaQueryWrapper<Category> lambdaQueryWrapper = new LambdaQueryWrapper<>();
//        根据类型查询
        lambdaQueryWrapper.eq(category.getType() != null,Category::getType,category.getType());
//        构造排序条件
        lambdaQueryWrapper.orderByAsc(Category::getSort).orderByDesc(Category::getUpdateTime);

        List<Category> list = categoryService.list(lambdaQueryWrapper);
        return R.success(list);
    }


}
