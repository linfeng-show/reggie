package com.lf.reggie.filter;

import com.alibaba.fastjson.JSON;
import com.lf.reggie.common.BaseContext;
import com.lf.reggie.domain.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @Author 林峰
 * @Date
 * @Version 1.0
 * 过滤器，检查是否登录
 * 关于过滤器详情请见本网站学习笔记
 */
@WebFilter(filterName = "loginCheckFilter", urlPatterns = "/*")
@Slf4j
public class LoginCheckFilter implements Filter {

    //    路径匹配器
    public static final AntPathMatcher PATH_MATCHER = new AntPathMatcher();

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

//        1.获取访问url
        String requestURI = request.getRequestURI();
//        System.out.println("访问路径" + requestURI);

//        2.设置放行路径,其中的路径不需要处理
        String[] urls = {"/employee/login", "/employee/logout",
                "/backend/**", "/front/**",
                "/favicon.ico","/common/**",
                "/user/sendMsg","/user/login"};
//        3.判断是否放行
        boolean b = checkUrl(urls, requestURI);
//        System.out.println(b);

        if (b) {
            filterChain.doFilter(request, response);
            return;
        }
//        4.判断用户是否登录
        HttpSession session = request.getSession();
        Long employee = (Long) session.getAttribute("employee");
//        5.成功则放行
        if (employee != null) {
            //            将用户登录id存入线程
            BaseContext.setCurrentId(employee);
            filterChain.doFilter(request, response);

            return;
        }
//        此处判断用户是否登录
        if (session.getAttribute("user") != null) {
            //            将用户登录id存入线程
            Long userId =  (Long)request.getSession().getAttribute("user");
            BaseContext.setCurrentId(userId);
            filterChain.doFilter(request, response);

            return;
        }
//       6.用户未登录，给前端返回错误信息
            response.getWriter().write((JSON.toJSONString(R.error("NOTLOGIN"))));
            return ;



    }

    boolean checkUrl(String[] urls, String requestUrl) {
        /**
         * @Description:
         * @Params: [urls, requestUrl]
         * urls:放行路径
         * requestUrl： 请求路径
         * @Return boolean
         * 判断是否放行
         */
        for (String url :
                urls) {
            boolean match = PATH_MATCHER.match(url, requestUrl);
            if (match) return true;
        }
        return false;

    }
}
