package com.lf.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lf.reggie.domain.ShoppingCart;

/**
 * @Author 林峰
 * @Date
 * @Version 1.0
 */
public interface ShoppingCartService extends IService<ShoppingCart> {
}
