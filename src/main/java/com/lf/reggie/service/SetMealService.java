package com.lf.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lf.reggie.domain.Setmeal;
import com.lf.reggie.dto.SetmealDto;

import java.util.List;

/**
 * @Author 林峰
 * @Date
 * @Version 1.0
 */
public interface SetMealService extends IService<Setmeal> {

//    保存菜品和套餐
    public void saveWithMeal(SetmealDto setmealDto);

//    删除套餐和关联菜品
    public void deleteWithMeal(List<Long> ids);
}
