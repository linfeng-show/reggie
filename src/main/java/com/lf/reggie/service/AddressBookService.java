package com.lf.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lf.reggie.domain.AddressBook;


/**
 * @Author 林峰
 * @Date
 * @Version 1.0
 */
public interface AddressBookService extends IService<AddressBook> {
}
