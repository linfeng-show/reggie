package com.lf.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lf.reggie.domain.Employee;

/**
 * @Author 林峰
 * @Date
 * @Version 1.0
 */
public interface EmployeeService extends IService<Employee> {
}
