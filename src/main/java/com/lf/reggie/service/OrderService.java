package com.lf.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lf.reggie.domain.Orders;
import com.lf.reggie.mapper.OrderMapper;

/**
 * @Author 林峰
 * @Date
 * @Version 1.0
 */
public interface OrderService extends IService<Orders> {


//    用户下单
    void submit(Orders orders);
}
