package com.lf.reggie.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lf.reggie.domain.ShoppingCart;
import com.lf.reggie.mapper.ShoppingCartMapper;
import com.lf.reggie.service.ShoppingCartService;
import org.springframework.stereotype.Service;

/**
 * @Author 林峰
 * @Date
 * @Version 1.0
 */
@Service
public class ShoppingCartServiceImpl extends ServiceImpl<ShoppingCartMapper, ShoppingCart> implements ShoppingCartService {
}
