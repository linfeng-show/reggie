package com.lf.reggie.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lf.reggie.domain.AddressBook;
import com.lf.reggie.mapper.AddressBookMapper;
import com.lf.reggie.service.AddressBookService;
import org.springframework.stereotype.Service;

/**
 * @Author 林峰
 * @Date
 * @Version 1.0
 */
@Service
public class AddressBookServiceImpl extends ServiceImpl<AddressBookMapper,AddressBook> implements AddressBookService {
}
