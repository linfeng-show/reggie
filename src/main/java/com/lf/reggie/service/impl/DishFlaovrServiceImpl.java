package com.lf.reggie.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lf.reggie.domain.DishFlavor;
import com.lf.reggie.mapper.DishFlavorMapper;
import com.lf.reggie.service.DishFlavorService;
import org.springframework.stereotype.Service;

/**
 * @Author 林峰
 * @Date
 * @Version 1.0
 */
@Service
public class DishFlaovrServiceImpl extends ServiceImpl<DishFlavorMapper, DishFlavor> implements DishFlavorService {
}
