package com.lf.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lf.reggie.common.CustomException;
import com.lf.reggie.domain.Category;
import com.lf.reggie.domain.Dish;
import com.lf.reggie.domain.Setmeal;
import com.lf.reggie.mapper.CategoryMapper;
import com.lf.reggie.service.CategoryService;
import com.lf.reggie.service.DishService;
import com.lf.reggie.service.SetMealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author 林峰
 * @Date
 * @Version 1.0
 */

@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements CategoryService {

    @Autowired
    private DishService dishService;

    @Autowired
    private SetMealService setMealService;

    @Override
    public void remove(Long id) {
/**
* @Description:
* @Params: [id]
 * 查询传进来的id
* @Return void
 * 自定义一个查询，因为业务要求我们如果删除项和其他项有关联，就不能删除
*/
//      查询条件：看是否有关联菜品
        LambdaQueryWrapper<Dish> dishLambdaQueryWrapper = new LambdaQueryWrapper<>();
        dishLambdaQueryWrapper.eq(Dish::getCategoryId,id);
//      如果数量大于0则说明有关联
        int count1 = dishService.count(dishLambdaQueryWrapper);

        if(count1 > 0){
//            抛异常
            throw new CustomException("删除失败，分类包含菜品");
        }

//        查询条件，看是否关联套餐
        LambdaQueryWrapper<Setmeal> setmealLambdaQueryWrapper = new LambdaQueryWrapper<>();
        setmealLambdaQueryWrapper.eq(Setmeal::getCategoryId,id);
//      数量大于0说明有关联
        int count2 = setMealService.count(setmealLambdaQueryWrapper);

        if(count2 > 0){
//            抛异常
            throw new CustomException("删除失败，分类包含套餐");
        }

        super.removeById(id);

    }
}
