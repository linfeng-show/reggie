package com.lf.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lf.reggie.common.CustomException;
import com.lf.reggie.domain.Setmeal;
import com.lf.reggie.domain.SetmealDish;
import com.lf.reggie.dto.SetmealDto;
import com.lf.reggie.mapper.SetMealMapper;
import com.lf.reggie.service.SetMealDishService;
import com.lf.reggie.service.SetMealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author 林峰
 * @Date
 * @Version 1.0
 */
@Service
public class SetMealServiceImpl extends ServiceImpl<SetMealMapper, Setmeal> implements SetMealService {

    @Autowired
    private SetMealDishService setMealDishService;

    @Override
    public void saveWithMeal(SetmealDto setmealDto) {
//        保存套餐基本信息
        this.save(setmealDto);
//        保存菜品和套餐关系
        List<SetmealDish> setmealDishes = setmealDto.getSetmealDishes();
        for(SetmealDish i:setmealDishes){
            i.setSetmealId(setmealDto.getId());
        }

        setMealDishService.saveBatch(setmealDishes);
    }

    @Override
    public void deleteWithMeal(List<Long> ids) {
//        先判断要删除的套餐里面有没有还在售的，有就报错
        LambdaQueryWrapper<Setmeal> lambdaQueryWrapper = new LambdaQueryWrapper<>();

        lambdaQueryWrapper.in(Setmeal::getId,ids);
        lambdaQueryWrapper.eq(Setmeal::getStatus,1);

        int count = this.count(lambdaQueryWrapper);
        if(count > 0){
            throw new CustomException("存在正在售卖正的套餐，删除失败，请重新选择");

        }
        this.removeByIds(ids);

        LambdaQueryWrapper<SetmealDish> lambdaQueryWrapper2 = new LambdaQueryWrapper<>();
        lambdaQueryWrapper2.in(SetmealDish::getSetmealId,ids);

        setMealDishService.remove(lambdaQueryWrapper2);
    }
}
