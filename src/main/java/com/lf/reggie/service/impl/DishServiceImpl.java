package com.lf.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lf.reggie.domain.Dish;
import com.lf.reggie.domain.DishFlavor;
import com.lf.reggie.dto.DishDto;
import com.lf.reggie.mapper.DishMapper;
import com.lf.reggie.service.DishFlavorService;
import com.lf.reggie.service.DishService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author 林峰
 * @Date
 * @Version 1.0
 */
@Service
public class DishServiceImpl extends ServiceImpl<DishMapper, Dish> implements DishService {

    @Autowired
    private DishFlavorService dishFlavorService;

    @Autowired
    private DishService dishService;



//    这里涉及了多张表，需要添加事务
    @Transactional
    public void saveWithFlavor(DishDto dishDto) {
        /**
        * @Description: 实现同时添加菜品和口味
        * @Params: [dishDto]
        * @Return void
        */
//        保存菜品
        this.save(dishDto);

        Long id = dishDto.getId();

        List<DishFlavor> flavors = dishDto.getFlavors();
//      添加菜品id
        for (DishFlavor item:flavors
             ) {
            item.setDishId(id);

        }
//        保存口味
        dishFlavorService.saveBatch(flavors);
    }

    @Transactional
    public DishDto getByIdWithFlavor(Long id) {

        DishDto dishDto = new DishDto();

        Dish dish = dishService.getById(id);

//        拦截器
        LambdaQueryWrapper<DishFlavor> queryWrapper = new LambdaQueryWrapper<>();

        queryWrapper.eq(DishFlavor::getDishId,id);

        List<DishFlavor> flavors = dishFlavorService.list(queryWrapper);

        BeanUtils.copyProperties(dish,dishDto);
        dishDto.setFlavors(flavors);

        return dishDto;
    }

//  更新菜品信息
    @Transactional
    public void updateWithFlavor(DishDto dishdto) {
//        更新dish表
        dishService.updateById(dishdto);

//        删除原先口味表里存在的口味数据
        LambdaQueryWrapper<DishFlavor> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(DishFlavor::getDishId,dishdto.getId());

        dishFlavorService.remove(queryWrapper);

//        重新添加口味信息
        dishFlavorService.saveBatch(dishdto.getFlavors());
    }
}
