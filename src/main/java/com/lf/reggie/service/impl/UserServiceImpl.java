package com.lf.reggie.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lf.reggie.domain.User;
import com.lf.reggie.mapper.UserMapper;
import com.lf.reggie.service.UserService;
import org.springframework.stereotype.Service;

/**
 * @Author 林峰
 * @Date
 * @Version 1.0
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
}
