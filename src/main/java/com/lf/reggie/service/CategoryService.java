package com.lf.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lf.reggie.domain.Category;

/**
 * @Author 林峰
 * @Date
 * @Version 1.0
 */
public interface CategoryService extends IService<Category> {
//    按分类查询
    public void remove(Long id);
}
