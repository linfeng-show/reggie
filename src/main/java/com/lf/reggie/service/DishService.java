package com.lf.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lf.reggie.domain.Dish;
import com.lf.reggie.dto.DishDto;


/**
 * @Author 林峰
 * @Date
 * @Version 1.0
 */
public interface DishService extends IService<Dish> {

//    新增方法，同时保存菜品和口味
    public void saveWithFlavor(DishDto dishDto);

//   新增方法，查询菜品信息和口味
    public DishDto getByIdWithFlavor(Long id);

//    新增方法，更新菜品信息和口味
    public void updateWithFlavor(DishDto dishdto);
}
