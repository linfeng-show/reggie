package com.lf.reggie.dto;


import com.lf.reggie.domain.Setmeal;
import com.lf.reggie.domain.SetmealDish;
import lombok.Data;
import java.util.List;

@Data
public class SetmealDto extends Setmeal {

    private List<SetmealDish> setmealDishes;

    private String categoryName;
}
